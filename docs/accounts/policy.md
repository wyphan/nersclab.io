# Account Policies

There are a number of policies which apply to NERSC users. These
policies originate from a number of sources, such as DOE regulations
and DOE and NERSC management.

## User Account Ownership, Password, and MFA Policies

A user is given a username (also known as a login name) and creates a
password and Multi-Factor Authentication (MFA) token that enable them 
to access NERSC resources. This username and its associated password 
and MFA may be used by a single individual only.

**Passwords may not be shared, and must be created or changed using
specified rules**. See [NERSC Passwords](passwords.md) for more
information.

Likewise, [MFA tokens](../connect/mfa/) must be kept secure and may 
not be shared with other users.

NERSC will disable a user who shares any one of their passwords with
another person. If a person authenticating to a username is not the
one who is officially registered with NERSC as the owner of that
username, then "sharing" has occurred and all usernames associated
with the owner of the shared username will be disabled.

If a user is disabled due to account sharing, the PI or an authorized
project manager must send a memo to accounts@nersc.gov explaining why
the sharing occurred and assuring that it will not occur again. NERSC
will then determine if the user should be re-enabled.

The computer use policies and security rules that apply to all users
of NERSC resources are listed in the 
[Appropriate Use Policy](https://www.nersc.gov/users/policies/appropriate-use-of-nersc-resources/) 
form.

## Security Incidents

If you think there has been a computer security incident you should
contact NERSC Security as soon as possible at security@nersc.gov.

Please save any evidence of the break-in and include as many details
as possible in your communication with us.

## Acknowledge use of NERSC resources

Please acknowledge NERSC in your publications, for example:

>This research used resources of the National Energy Research
>Scientific Computing Center, which is supported by the Office of
>Science of the U.S. Department of Energy under Contract
>No. DE-AC02-05CH11231.
