# Getting Started

!!! success "About this page"
	This document will guide you through the basics of using NERSC's
	supercomputers, storage systems, and services.
	
Welcome to the National Energy Research Scientific Computing Center
(NERSC)! If you are a new or existing user, please see links to slides and video for overview of NERSC.  
 
* [NERSC Overview Slides](https://www.nersc.gov/assets/Uploads/01-Overview-20200616.pdf)
* [NERSC Overview Video Recording](https://www.youtube.com/watch?v=NfEUJsENtnA&list=PL20S5EeApOSsFDfNrshVdiaQKZK4CNFq7&index=1)

## Computing Resources

### Perlmutter

Perlmutter is a [HPE Cray
EX](https://support.hpe.com/hpesc/public/docDisplay?docId=a00109703en_us&docLocale=en_US)
supercomputer that is currently being built. Its phase 1 system has
over 1500 GPU-accelerated compute nodes.

* [Detailed Perlmutter system specifications](./systems/perlmutter/system_details.md)

### Cori

Cori is a [Cray XC40](https://www.hpe.com/us/en/what-is/cray-xc-series.html)
supercomputer with approximately 12000 compute nodes.

* [Detailed Cori system specifications](./systems/cori/index.md)
* [NERSC Live status](https://www.nersc.gov/live-status/motd/)

## Storage Resources

File systems are configured for different purposes. Each machine has access
to at least three different file systems with different levels of performance,
permanence and available space.

* [NERSC File System Overview](filesystems/index.md)
* [NERSC File Systems Slides](https://www.nersc.gov/assets/Uploads/09-Filesystems-20200616.pdf)
* [NERSC File Systems Video](https://www.youtube.com/watch?v=B9ceCtmM6bg&list=PL20S5EeApOSsFDfNrshVdiaQKZK4CNFq7&index=9)
* [I/O Best Practices Slides](https://www.nersc.gov/assets/Uploads/11-IO-Best-Practices-20200616.pdf)
* [I/O Best Practices Video](https://www.youtube.com/watch?v=pP4vkts-t18&list=PL20S5EeApOSsFDfNrshVdiaQKZK4CNFq7&index=11)

### Community File System (CFS)

The [Community File System (CFS)](./filesystems/community.md) is a
global file system available on all NERSC computational systems. It
allows sharing of data between users, systems, and the "outside
world".

* [Detailed CFS usage information](./filesystems/community.md)

### High Performance Storage System (HPSS) Archival Storage

The High Performance Storage System (HPSS) is a modern, flexible,
performance-oriented mass storage system. It has been used at NERSC
for archival storage since 1998. HPSS is intended for long term
storage of data that is not frequently accessed.

* [Detailed HPSS usage information](./filesystems/archive.md)

## NERSC Accounts

In order to use the NERSC facilities, you need to [obtain a NERSC account](./accounts/index.md#obtaining-an-account)
and your user account must be tied to a [NERSC Allocation](https://www.nersc.gov/users/accounts/allocations/) in order 
to run jobs. Please see our [password rules](./accounts/passwords.md) if you need help creating account
or recovering passwords. Once you have account, you can login to [Iris](https://iris.nersc.gov) to manage 
account details.

For more details on account support see following links below:

* [Accounts and Allocations Slides](https://www.nersc.gov/assets/Uploads/02-AccountsAllocations-20200616.pdf)
* [Accounts and Allocations Video](https://www.youtube.com/watch?v=RFou_rc3SuA&list=PL20S5EeApOSsFDfNrshVdiaQKZK4CNFq7&index=2)

!!! tip "With [Iris](https://iris.nersc.gov) you can"
	* Check allocation balances
	* Change passwords
	* Run reports
	* Update contact information
	* Clear login failures
	* Change login shell
	* and more!

## Connecting to NERSC

!!! Check "MFA is *required* for NERSC users"

* [Multi-Factor Authentication (MFA)](./connect/mfa.md)
* [SSH](./connect/index.md#ssh)
* [Login nodes](./connect/index.md)
* [Troubleshooting connection problems](./connect/index.md#troubleshooting)
* [NERSC Live status](https://www.nersc.gov/live-status/motd/)
* [Connecting to NERSC Slides](https://www.nersc.gov/assets/Uploads/03-Connecting-to-NERSC-20200616.pdf)
* [Connecting to NERSC Video](https://www.youtube.com/watch?v=qS50aFplA8M&list=PL20S5EeApOSsFDfNrshVdiaQKZK4CNFq7&index=3)

## Software

NERSC and its vendors supply a rich set of compilers, HPC utilities,
programming libraries, development tools, debuggers/profilers, data and visualization tools.

We provide a list of [application](./applications/index.md) specific documentation that are built
by NERSC staff. NERSC software stack will be provided via [spack](https://spack.readthedocs.io/en/latest/)
package manager that helps automate software builds for popular HPC tools. To learn more about our setup see 
[NERSC Spack Setup](./development/build-tools/spack.md).

NERSC will provide software stack via [Extreme-Scale Scientific Software Stack (E4S)](https://e4s-project.github.io/) which 
is a collection of open source software packages for HPC system built via spack. NERSC will provide
periodic updates (quarterly/semi-annual) for [e4s stacks](applications/e4s/index.md) which can be accessed via `module load e4s`.

Software can be accessed via `module` command current we support [lmod](./environment/lmod.md)
for Perlmutter, and [environment-modules](./environment/modules.md) for Cori.

Please see our [Software Policy](./policies/software-policy/index.md) outlining our software support model 
for NERSC provided software stack.  

!!! question "Something missing?"
	If there is something missing that you would like to have on
	our systems, please [submit a request](https://help.nersc.gov) and
	we will evaluate it for appropriateness, cost, effort, and benefit
	to the community.
	
### Python 

The Python we provide at NERSC is [Anaconda Python](https://docs.anaconda.com/anaconda/user-guide/getting-started/).

* [Brief introduction to Python at NERSC](development/languages/python/index.md)
* [Python at NERSC Slides](https://www.nersc.gov/assets/Uploads/12-Python-20200616.pdf)
* [Python at NERSC Video](https://www.youtube.com/watch?v=73l1wvU5uq0&list=PL20S5EeApOSsFDfNrshVdiaQKZK4CNFq7&index=12)

### Machine Learning

NERSC supports a variety of software for Machine Learning and Deep Learning on our systems.

* [Machine Learning at NERSC](machinelearning/index.md)
* [Deep Learning Stack at NERSC Slides](https://www.nersc.gov/assets/Uploads/15-DeepLearning-20200616.pdf)
* [Deep Learning Stack at NERSC Video](https://www.youtube.com/watch?v=7Jy_9sky28s&list=PL20S5EeApOSsFDfNrshVdiaQKZK4CNFq7&index=15)

## Computing Environment

!!! info
	[`$HOME` directories](./filesystems/global-home.md) are shared
	across all NERSC systems (except HPSS)

## Compiling/ building software

* [Compilers at NERSC](./development/compilers/wrappers.md)
* [Shell Environment](./environment/index.md)
* [Programming Environment and Compilers Slides](https://www.nersc.gov/assets/Uploads/04-PE-Compilations-20200616.pdf)
* [Programming Environment and Compilers Video](https://www.youtube.com/watch?v=NLScEGsz_rk&list=PL20S5EeApOSsFDfNrshVdiaQKZK4CNFq7&index=4)

## Running Jobs

Typical usage of the system involves submitting scripts (also
referred to as "jobs") to a batch system such as
[Slurm](https://slurm.schedmd.com/).

* [Overview of jobs at NERSC](./jobs/index.md)
* [Rich set of example jobs](./jobs/examples/index.md)
* [Running Jobs Slides](https://www.nersc.gov/assets/Uploads/05-Running-Jobs-20200616.pdf)
* [Running Jobs Video](https://www.youtube.com/watch?v=01AaFOqwzQE&list=PL20S5EeApOSsFDfNrshVdiaQKZK4CNFq7&index=5)
* [Workflows Slides](https://www.nersc.gov/assets/Uploads/08-Workflows-20200616.pdf)
* [Workflows Video](https://www.youtube.com/watch?v=hU3YoIJ5Tgo&list=PL20S5EeApOSsFDfNrshVdiaQKZK4CNFq7&index=8)

## Interactive Computing

NERSC provides interactive computing via [interactive slurm jobs](./jobs/interactive.md) and 
[Jupyter](./services/jupyter.md) service. 

* [Using Jupyter Slides](https://www.nersc.gov/assets/Uploads/13-Using-Jupyter-20200616.pdf)
* [Using Jupyter Video](https://www.youtube.com/watch?v=uoKSmCvTk2s&list=PL20S5EeApOSsFDfNrshVdiaQKZK4CNFq7&index=13)

## Debugging and Profiling

NERSC provides many popular debugging and profiling tools.

* [Performance Tools](tools/performance/index.md)
* [Debugging Tools](tools/debug/index.md)
* [Debugging Tools Slides](https://www.nersc.gov/assets/Uploads/06a-DebuggingTools-20200616.pdf)
* [Profiling Tools Slides](https://www.nersc.gov/assets/Uploads/06b-ProfiingTools-20200616.pdf)

## Data Ecosystem

NERSC Production Data Stack includes support for Data Transfer +
Access, Workflows, Data Management Tools, Data Analytics, and Data
Visualization.

* [Data Ecosystem Overview Slides](https://www.nersc.gov/assets/Uploads/07-Data-Ecosystem-Overview-20200616.pdf)
* [Data Ecosystem Overview Video](https://www.youtube.com/watch?v=CgzK9fNNc6s&list=PL20S5EeApOSsFDfNrshVdiaQKZK4CNFq7&index=7)

## Data Sharing

### Security and Data Integrity

Sharing data with other users must be done carefully. Permissions
should be set to the minimum necessary to achieve the desired
access. For instance, consider carefully whether it's really necessary
before sharing write permissions on data. Be sure to have archived
backups of any critical shared data. It is also important to ensure
that private login secrets (like SSH private keys or apache
htaccess files) are not shared with other users (either intentionally
or accidentally). Good practice is to keep things like this in a
separate directory that is as locked down as possible.

### Sharing with Other Members of Your Project

NERSC's [Community file system](filesystems/community.md) is set up
with group read and write permissions and is ideal for sharing with
other members of your project. There is a directory for every active
project at NERSC and all members of that project should have access to
it by default.

### Sharing with NERSC Users Outside of Your Project

You can share files and directories with NERSC users outside of your
project by adjusting the unix file permissions. We have an [extensive
write-up of unix file
permissions](filesystems/unix-file-permissions.md) and how they work.

NERSC provides two commands: `give` and `take` which are useful for
sharing small amounts of data between users.

To send a file or path to `<receiving_username>`:

```
give -u <receiving_username> <file or directory>
```

To receive a file sent by `<sending_username>`:

```
take -u <sending_username> <filename>
```

To take all files from `<sending_username>`:

```
take -a -u <sending_username>
```

To see what files `<sending_username>` has sent to you:

```
take -u <sending_username>
```

For a full list of options pass the `--help` flag.

!!! warning
    Files that remain untaken 12 weeks after being given will be
    purged from the staging area.

### Sharing Data outside of NERSC

You can easily and quickly share data over the web using our [Science
Gateways](services/science-gateways.md) framework.

You can also share large volumes of data externally by setting up a
[Globus Sharing Endpoint](services/globus.md#globus-sharing).

## Data Transfers

NERSC partners with ESNet to provide a high speed connection to the
outside world. NERSC also provides several tools and systems optimized
for data transfer.

* [Data Transfer Best Practices Slides](https://www.nersc.gov/assets/Uploads/10-Data-Transfer-Best-Practices-20200616.pdf)
* [Data Transfer Best Practices Video](https://www.youtube.com/watch?v=r54vWJtc6h8&list=PL20S5EeApOSsFDfNrshVdiaQKZK4CNFq7&index=10)

### External Data Transfer

!!! tip
    **NERSC recommends transferring data to and from
    NERSC using [Globus](services/globus.md)**

[Globus](services/globus.md) is a web-based service that solves
many of the challenges encountered moving data between systems. Globus
provides the most comprehensive, efficient, and easy to use
service for most NERSC users.

However, there are other tools available to transfer data between
NERSC and other sites:

* [scp](services/scp.md): standard Linux utilities suitable for smaller files (<1GB)
* [GridFTP](services/gridftp.md): parallel transfer software for large files

### Transferring Data Within NERSC

!!! tip
    **"Do you need to transfer at all?"**  If your data is on NERSC
    Global File Systems (`/global/cfs`, `/global/projecta`,
    `/global/cscratch`), data transfer may not be necessary because
    these file systems are mounted on almost all NERSC
    systems. However, if you are doing a lot of I/O with these files,
    you will benefit from staging them on the most performant file
    system. Usually that's the local scratch file system or the Burst
    Buffer.

* Use the the unix command `cp`, `tar` or `rsync` to copy files within
   the same computational system. For large amounts of data use Globus
   to leverage the automatic retry functionality

### Data Transfer Nodes

The [Data Transfer Nodes (DTNs)](systems/dtn/index.md) are servers
dedicated for data transfer based upon the ESnet Science DMZ
model. DTNs are tuned to transfer data efficiently, optimized for
bandwidth and have direct access to most of the NERSC file
systems. These transfer nodes are configured within Globus as managed
endpoints available to all NERSC users.

### NERSC FTP Upload Service

NERSC maintains [an FTP upload
service](https://www.nersc.gov/users/storage-and-file-systems/nersc-ftp-upload-service/)
designed for external collaborators to be able to send data to NERSC
staff and users.

## Getting Help

NERSC places a very strong emphasis on enabling science and providing
user-oriented systems and services. 

### Help Desk

!!! info "Availability"
	Account support is available 8-5 Pacific Time on business days.

The [online help desk](https://help.nersc.gov/) is the **preferred**
method for contacting NERSC.

!!! attention
	NERSC Consultants handle thousands of support requests per
	year. In order to ensure efficient timely resolution of issues
	include **as much of the following as possible** when making a
	request:

	* error messages
	* jobids
	* location of relevant files
	     * input/output
	     * job scripts
	     * source code
	     * executables
	* output of `module list`
	* any steps you have tried
	* steps to reproduce

!!! tip
    You can make code snippets, shell outputs, etc in your ticket much more 
    readable by inserting a line with:

    ```
    [code]<pre>
    ```

    before the snippet, and another line with:

    ```
    </pre>[/code]
    ```

    after it. While these are the most useful, other options to improve
    formatting can be found in the [full list of formatting
    options](https://community.servicenow.com/community?id=community_blog&sys_id=4d9ceae1dbd0dbc01dcaf3231f9619e1).

Access to the online help system requires logging in with your NERSC username,
password, and one-time password. If you are an existing user unable to log in,
you can send an email to <accounts@nersc.gov> for support.

If you are not a NERSC user, you can reach NERSC with your queries at
`accounts@nersc.gov` or `allocations@nersc.gov`.

### Operations

!!! warning "For *critical* system issues only."

* Please check the [Online Status Page](https://www.nersc.gov/live-status/motd/)
 before calling 1-800-666-3772 (USA only) or 1-510-486-8600, Option 1

### Phone support

**Consulting and account-support phone services have been suspended.**

To report an urgent system issue, you may call NERSC at 1-800-66-NERSC
(USA) or 510-486-8600 (local and international).

### FAQ

Please see the following links for common issues that can be addressed, if you are still having 
issues please create a ticket in [help desk](https://help.nersc.gov). 

* [Password Resets](accounts/passwords.md#forgotten-passwords)
* [Connection problems](connect/index.md#ssh)
* [File Permissions](filesystems/unix-file-permissions.md)
* [Troubleshooting Slurm](jobs/troubleshooting.md)

### Appointments with NERSC User-Support Staff

NERSC provides 25-minute appointments with NERSC expert staff.  Before
you [schedule your appointment](https://nersc.as.me) consult the list
of available topics described below.

!!! tip
    To make the most use of an appointment, we **strongly
    encourage** you to try some things on your own and share them with
    NERSC staff ahead of time using the appointment intake form.

!!! attention
    Appointments use Google Hangouts for video conferencing. If you are using
    Google Chrome, please make sure that the “enable screen share” Security
    setting is enabled.

#### NERSC 101

This category is good for basic questions, and you could not find the answer in
our documentation. Or when you just don't know where to start.

#### KNL Optimization

Advice on how to optimize code and compilers to make use of the KNL compute
nodes on Cori. Possible discussion topics include:

1. Compiling code
2. Thread affinity
3. Batch script setup
4. Profiling your code
5. Refactoring your code

#### Containers at NERSC

Advice on deploying containerized workflows at NERSC using Shifter. We
recommend that you share your Dockerfile, the image name (after downloading it
to Cori using `shifterimg`) before the appointment if possible.

#### NERSC Filesystems

Advice on I/O optimization and Filesystems at NERSC. Possible discussion topics
include:

1. Optimal file system choices
2. Quota and file-permission issues
3. I/O profiling
4. Refactoring your code

#### GPU Basics

Advice on programming GPUs for users that are new to the topic. This category
is good for when you have started developing your GPU code, but are
encountering problems.

#### Using GPUs in Python

Advice on how to use GPUs from Python, eg. `CuPy`, `RAPIDS`, or `PyCUDA`.

#### Checkpoint/Restart using MANA

Advice on how to use MANA to enable automatic
[checkpoint/restart](development/checkpoint-restart/index.md) in
MPI applications.
