cori$ dmtcp_command --help
Usage:  dmtcp_command [OPTIONS] COMMAND [COMMAND...]
Send a command to the dmtcp_coordinator remotely.

Options:

  -h, --coord-host HOSTNAME (environment variable DMTCP_COORD_HOST)
              Hostname where dmtcp_coordinator is run (default: localhost)
  -p, --coord-port PORT_NUM (environment variable DMTCP_COORD_PORT)
              Port where dmtcp_coordinator is run (default: 7779)
  --help
              Print this message and exit.
  --version
              Print version information and exit.

Commands for Coordinator:
    -s, --status:          Print status message
    -l, --list:            List connected clients
    -c, --checkpoint:      Checkpoint all nodes
    -bc, --bcheckpoint:    Checkpoint all nodes, blocking until done
    -i, --interval <val>   Update ckpt interval to <val> seconds (0=never)
    -k, --kill             Kill all nodes
    -q, --quit             Kill all nodes and quit

Report bugs to: dmtcp-forum@lists.sourceforge.net
DMTCP home page: <http://dmtcp.sourceforge.net>

