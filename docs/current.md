# Current known issues

## Perlmutter

Please keep in mind that Perlmutter is under active
development. Access to the storage systems (scratch, CFS, and HPSS) as
well as to the outside world may be slow or hang unexpectedly.

NERSC has automated monitoring that tracks failed nodes, so please
only open tickets for node failures if the node consistently has poor
performance relative to other nodes in the job or if the node
repeatedly causes your jobs to fail.

- Batch system email notifications for jobs (including those for
  `scrontab`) are currently not enabled
- `PrgEnv-gnu` users **must** load `cpe-cuda` in order to get
  compatible versions of `gcc` and `cuda` or `cudatoolkit`. The
  correct order is:

    ```shell
    module load PrgEnv-gnu
    module load cpe-cuda
    module load cuda
    ```

- Static compiling doesn’t work, only dynamic linking works.
- CUDA-aware MPICH can only use up to half the resources on a CPU when
  it can see a GPU due to allocating GPU memory. Setting GPU binding will help
  distribute the CUDA-objects between GPUs to avoid an out of memory
  error.
- The lmod configuration for csh doesn't carry over to non-interactive
  logins (like batch scripts). This can be worked around by adding
  `source /usr/share/lmod/8.3.1/init/csh` to your `.tcshrc` file.
- Some users may see messages like `-bash:
  /usr/common/usg/bin/nersc_host: No such file or directory` when you
  login. This means you have outdated dotfiles that need to be
  updated. To stop this message, you can either delete this line from
  your dot files or check if `NERSC_HOST` is set before overwriting
  it. Please see our [environment
  page](environment/index.md#home-directories-shells-and-dotfiles)
  for more details.
- Using openmpi in shifter requires `--mpi=pmi2`
- Users sometimes encounter a `CUDA Unknown Error` during initialization.
- Machine learning applications (See [ML issues page](machinelearning/known_issues.md) for workaround):
    - Users sometimes encounter a `CUDA Unknown Error` during initialization.
    - Some Nvidia ngc containers don't properly enter compatibility mode when running with shifter.
- Users may notice MKL-based CPU code runs more slowly. Try
  `module load fast-mkl-amd`.
- Nodes on Perlmutter currently do not get a constant `hostid` (IP address) response.
- One might note a nvc/nvc++ warning if compiling your code with the Cray wrappers in PrgEnv-nvidia
`nvc-Warning-The -gpu option has no effect unless a language-specific option to enable GPU code generation
 is used (e.g.: -acc, -mp=gpu, -stdpar, -cuda)`. Currently there is no workaround,
however one can safely ignore the `-gpu` warning.
- If one notices a compile error with PrgEnv-nvidia as such:
  `/usr/bin/ld: cannot find -lcudart` we recommend you please add this library path to your compile line
 ( -L /global/common/software/nersc/cos1.3/cuda/11.3.0/targets/x86_64-linux/lib).
- MPI/mpi4py users may notice a
  [mlx5 error](development/languages/python/using-python-perlmutter.md#known-issues)
  error that stems from spawning forks within an MPI rank, which is considered
  undefined/unsupported behavior.

!!! caution "Be careful with NVIDIA Unified Memory to avoid crashing nodes"
    In your code, [NVIDIA Unified Memory](https://developer.nvidia.com/blog/unified-memory-cuda-beginners/)
    might
    look something like `cudaMallocManaged`. At the moment, we do not have
    the ability to control this kind of memory and keep it under a safe
    limit. Users who allocate a large
    pool of this kind of memory may end up crashing nodes if the UVM
    memory does not leave enough room for necessary system tools like
    our filesystem client.
    We expect a fix in early 2022. In the meantime, please keep the size
    of memory pools allocated via UVM relatively small. If you have
    questions about this, please contact us.
